package com.luban.ziya.classload;

import org.openjdk.jol.info.ClassLayout;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Created By ziya
 * QQ: 3039277701
 * 2021/4/8
 */
public class Test_31 {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Test_31 obj = new Test_31();

        Field field = Unsafe.class.getDeclaredField("theUnsafe");
        field.setAccessible(true);
        Unsafe unsafe = (Unsafe) field.get(null);

        unsafe.monitorEnter(obj);
//        System.out.println(ClassLayout.parseInstance(obj).toPrintable());
        System.out.println("ziya");
        unsafe.monitorExit(obj);
    }

}
