package com.luban.ziya.sync;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/9/14
 */
public class SyncTest_17 {

    public int v = 0;

    public static void main(String[] args) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);

        SyncTest_17 obj = new SyncTest_17();

//        while (true) {
            obj.test();
//        }
    }

    public synchronized void test() {
        System.out.println(v++);
        test();
    }
}
